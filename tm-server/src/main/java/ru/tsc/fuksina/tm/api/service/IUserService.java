package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.dto.model.UserDTO;

public interface IUserService extends IService<UserDTO> {

    @Nullable
    UserDTO findOneByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @Nullable
    UserDTO findOneByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    void removeByLogin(@Nullable String login);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDTO updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
